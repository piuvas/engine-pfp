use sdl2::event::{Event, WindowEvent};
use sdl2::image::LoadTexture;
use sdl2::keyboard::Scancode;
use sdl2::pixels::Color;
use sdl2::rect::Rect;

use tiled::Loader;

use std::collections::HashMap;
use std::error::Error;
use std::fs;
use std::path::PathBuf;
use std::time::{Duration, Instant};

struct Game {
    // character coordinates,
    char_x: i32,
    char_y: i32,
    // camera offset.
    offset_x: i32,
    offset_y: i32,

    screen_width: i32,
    screen_height: i32,

    zoom: i32,
    rect_size: u32,
    direction: Direction,
}
impl Game {
    fn new(width: i32, height: i32) -> Self {
        Game {
            char_x: 0,
            char_y: 0,
            screen_width: width,
            screen_height: height,
            offset_x: width / 2,
            offset_y: height / 2,
            zoom: 0,
            rect_size: 0,
            direction: Direction::South,
        }
    }
    fn adapt_size(&mut self, width: i32, height: i32) {
        self.screen_width = width;
        self.screen_height = height;
        let area = width * height;
        let mean = (area as f32).sqrt() as u32;
        let size = mean / 160;
        self.zoom = size as i32;
        self.rect_size = size * 16;
    }
    fn center_character(&mut self) {
        let half_rect = self.rect_size as i32 / 2;
        // assume character is 1 rect tall.
        let char_offset_x = (self.char_x * self.zoom) + half_rect;
        let char_offset_y = (self.char_y * self.zoom) + half_rect;

        // calculate where the camera is supposed to be.
        let cam_offset_x = self.screen_width / 2 - char_offset_x;
        let cam_offset_y = self.screen_height / 2 - char_offset_y;

        // distance from the current camera location to where it's supposed to be.
        let cam_dist_x = cam_offset_x - self.offset_x;
        let cam_dist_y = cam_offset_y - self.offset_y;

        // make velocity a function of the distance.
        let cam_vel_x = cam_dist_x / 8;
        let cam_vel_y = cam_dist_y / 8;

        self.offset_x += cam_vel_x;
        self.offset_y += cam_vel_y;
    }
}
struct GameTile {
    x: i32,
    y: i32,
    texture: PathBuf,
    col: bool,
}

enum Direction {
    North,
    South,
    West,
    East,
}

const CARINHA: &[u8] = include_bytes!("../assets/Slice 131.png");

fn main() -> Result<(), Box<dyn Error + Send + Sync>> {
    // start sdl context.
    let sdl_context = sdl2::init()?;
    let video_subsystem = sdl_context.video()?;

    let width = 800;
    let height = 600;

    let window = video_subsystem
        .window("teste", width as u32, height as u32)
        .position_centered()
        .resizable()
        .allow_highdpi()
        .build()?;

    let mut canvas = window.into_canvas().accelerated().present_vsync().build()?;
    canvas.set_logical_size(800, 600)?;
    let texture_creator = canvas.texture_creator();
    let mut event_pump = sdl_context.event_pump()?;

    let carinha = texture_creator.load_texture_bytes(CARINHA)?;

    let mut game = Game::new(width, height);
    game.adapt_size(width, height);
    game.center_character();

    canvas.set_draw_color(Color::GRAY);
    canvas.clear();
    canvas.present();

    let mut delta = Instant::now();

    // start tiled context.

    let mut loader = Loader::new();
    let map = loader.load_tmx_map("tiled-proj/mapa.tmx")?;

    // read level data.
    let mut gametiles: Vec<GameTile> = Vec::new();
    let mut texture_map: HashMap<PathBuf, Vec<u8>> = HashMap::new();

    let layers = map.layers();
    for layer in layers {
        if let Some(tile_layer) = layer.as_tile_layer() {
            let width = tile_layer.width().unwrap() as i32;
            let height = tile_layer.height().unwrap() as i32;
            for i in 0..width {
                for j in 0..height {
                    if let Some(tile) = tile_layer.get_tile(i, j) {
                        let origin_tileset = tile.get_tileset();
                        // get tile image from tileset.
                        let img = origin_tileset.image.as_ref().unwrap();
                        let img_path = &img.source;

                        // preload textures.
                        if !texture_map.contains_key(img_path) {
                            let bytes = fs::read(img_path)?;
                            texture_map.insert(img_path.clone(), bytes.clone());
                        };
                        let x = i;
                        let y = j;
                        let col = match layer.properties.get("col") {
                            Some(&tiled::PropertyValue::BoolValue(col)) => col,
                            _ => false,
                        };
                        gametiles.push(GameTile {
                            x,
                            y,
                            texture: img_path.clone(),
                            col,
                        });
                    }
                }
            }
        }
        if let Some(obj_layer) = layer.as_object_layer() {
            for object in obj_layer.objects() {
                match object.name.as_str() {
                    "char" => {
                        game.char_x = object.x as i32;
                        game.char_y = object.y as i32;
                    },
                    _ => {}
                }
            }
        }
    }

    'running: loop {
        // physics (fixed timestep)
        while delta.elapsed() >= Duration::from_secs_f64(1.0 / 60.0) {
            delta += Duration::from_secs_f64(1.0 / 60.0);
            for event in event_pump.poll_iter() {
                match event {
                    Event::Window {
                        win_event: WindowEvent::Resized(x, y),
                        ..
                    } => {
                        canvas.set_logical_size(x as u32, y as u32)?;
                        game.adapt_size(x, y);
                    },
                    Event::Quit { .. } => break 'running,
                    _ => {}
                }
            }

            let keyboard_state = event_pump.keyboard_state();
            for state in keyboard_state.pressed_scancodes() {
                match state {
                    Scancode::Up => {
                        let mut col = false;
                        for tile in &gametiles {if char_col(&game, tile, Direction::North) {col = true; break;}}
                        if !col {
                            game.direction = Direction::North;
                            game.char_y -= 1;
                        }
                    }
                    Scancode::Down => {
                        let mut col = false;
                        for tile in &gametiles {if char_col(&game, tile, Direction::South) {col = true; break;}}
                        if !col {
                            game.direction = Direction::South;
                            game.char_y += 1;
                        }
                    }
                    Scancode::Left => {
                        let mut col = false;
                        for tile in &gametiles {if char_col(&game, tile, Direction::West) {col = true; break;}}
                        if !col {
                            game.direction = Direction::West;
                            game.char_x -= 1;
                        }
                    }
                    Scancode::Right => {
                        let mut col = false;
                        for tile in &gametiles {if char_col(&game, tile, Direction::East) {col = true; break;}}
                        if !col {
                            game.direction = Direction::East;
                            game.char_x += 1;
                        }
                    }
                    _ => {}
                }
            }
        }
        // centering character under variable timestep to get smoother camera movement.
        game.center_character();

        // rendering (variable timestep)

        canvas.clear();

        for tile in &gametiles {
            if let Some(bytes) = texture_map.get(&tile.texture) {
                let texture = texture_creator.load_texture_bytes(bytes)?;
                let rect = Rect::new(
                    game.offset_x + (tile.x * game.rect_size as i32),
                    game.offset_y + (tile.y * game.rect_size as i32),
                    game.rect_size,
                    game.rect_size
                );
                canvas.copy(&texture, None, Some(rect))?;
            }
        }

        let player = Rect::new(
            game.offset_x + (game.char_x * game.zoom),
            game.offset_y + (game.char_y * game.zoom),
            game.rect_size,
            game.rect_size,
        );
        canvas.copy(&carinha, None, Some(player))?;
        canvas.present();
    }

    Ok(())
}

fn char_col(game : &Game, tile: &GameTile, direction: Direction) -> bool {
    // exit early if tile has no collision.
    if !tile.col { return false };
    let x = match direction {
        Direction::West => game.char_x - 1,
        Direction::East => game.char_x + 1,
        _ => game.char_x
    };
    let y = match direction {
        Direction::North => game.char_y - 1,
        Direction::South => game.char_y + 1,
        _ => game.char_y
    };
    let char_rect = Rect::new(x, y, 16, 16);
    let col_rect = Rect::new(tile.x * 16, tile.y * 16, 16, 16);
    char_rect.has_intersection(col_rect)
}
